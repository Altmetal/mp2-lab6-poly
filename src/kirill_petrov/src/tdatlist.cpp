// Copyright 2016 Petrov Kirill

#include "tdatlist.h"

// ������� ������

PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink)
{
	return new TDatLink(pVal, pLink);
}

void TDatList::DelLink(PTDatLink pLink)
{
	if (pLink != NULL)
	{
		if (pLink->pValue != NULL)
			delete pLink->pValue;
		delete pLink;
	}
}

// �����������

TDatList:: TDatList():ListLen (0)
{
	pFirst = pLast = pStop = NULL;
	// ������������� ������� ����� �������
	Reset();
}

// ������� � ���������

PTDatValue TDatList::GetDatValue(TLinkPos mode) const
{
	PTDatLink temp=NULL;
	switch (mode)
	{
	case FIRST: temp = pFirst; break;
	case CURRENT: temp = pCurrLink; break;
	case LAST: temp = pLast; break;
	}
	return (temp == NULL) ? NULL : temp->GetDatValue();
}

// ���������

int TDatList::GetCurrentPos(void) const
{
	return CurrPos;
}

int TDatList::Reset(void)
{
	pPrevLink = pStop;
	IsEmpty() ? (CurrPos = -1, pCurrLink = pStop) : (CurrPos = 0, pCurrLink = pFirst);
	return 0;
}

int TDatList::GoNext(void)
{
	return IsListEnded() ? 0 : pPrevLink = pCurrLink, pCurrLink = pCurrLink->GetNextDatLink(), CurrPos++, 1;
}

int TDatList::SetCurrentPos(int pos)
{
	Reset();
	for (int i = 0; i < pos; i++, GoNext())
		;
	return 0;
}

int TDatList::IsListEnded(void) const
{
	return pCurrLink == pStop;
}

//������� ���������

void TDatList:: InsFirst(PTDatValue pVal)
{
	PTDatLink temp = GetLink(pVal, pFirst);
	if (temp)
	{
		temp->SetNextLink(pFirst);
		pFirst= temp;
		ListLen++;
		if (ListLen == 1)
		{
			pLast = temp;
			Reset();
		}
		else if (CurrPos == 0)
		// ���� ���������� � ����� ������, ���� ������������� ������� �����
			pCurrLink = temp;
		else
			CurrPos++;
	}
}

void TDatList:: InsLast(PTDatValue pVal)
{
	PTDatLink temp = GetLink(pVal, pStop);
	if (temp)
	{
		if (pLast)
			pLast->SetNextLink(temp);
		pLast = temp;
		ListLen++;
		if (ListLen == 1)
		{
			pFirst = temp;
			Reset();
		}
		if (IsListEnded())
		// ���� ������� ����� ���� ���������, �� ���� ������� ��� ���������
			pCurrLink = temp;
	}
}

void TDatList:: InsCurrent(PTDatValue pVal)
{
	if ((pCurrLink == pFirst) || IsEmpty())
		InsFirst(pVal);
	else if (IsListEnded())
			InsLast(pVal);
	else
	{
		PTDatLink temp = GetLink(pVal, pCurrLink);
		if (temp)
		{
			pPrevLink->SetNextLink(temp);
			temp->SetNextLink(pCurrLink);
			pCurrLink = temp;
			ListLen++;
		}
	}
}

// �������� ���������

void TDatList:: DelFirst(void)
{
	if (!IsEmpty())
	{
		PTDatLink temp = pFirst;
		pFirst = pFirst->GetNextDatLink();
		DelLink(temp);
		ListLen--;
		if (IsEmpty())
		{
			pLast = pStop;
			Reset();
		}
		else if (CurrPos == 0) pCurrLink = pFirst;
		else if (CurrPos == 1) pPrevLink = pStop;
		if (CurrPos) CurrPos--;
	}
}

void TDatList::DelCurrent(void)
{
	if (pCurrLink)
	{
		if ((pCurrLink == pFirst) || IsEmpty())
			DelFirst();
		else
		{
			PTDatLink temp = pCurrLink;
			pCurrLink = pCurrLink->GetNextDatLink();
			pPrevLink->SetNextLink(pCurrLink);
			DelLink(temp);
			ListLen--;
			// � ��� ���� ����� ���������???
			if (pCurrLink == pLast)
			{
				pLast = pPrevLink;
				pCurrLink = pStop;
			}
		}
	}
}

void TDatList::DelList(void)
{
	while (!IsEmpty()) DelFirst();
	pFirst = pLast = pPrevLink = pCurrLink = pCurrLink = pStop;
	CurrPos = -1;
}